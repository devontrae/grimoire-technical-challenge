const express = require('express')

// Server setup
const app = express()
const port = process.env.port || 3000
const cors = require('cors')
const path = require('path')

// Default App Middleware
app.use(express.static('dist'))

app.use(cors())

// Include the Censoring API
const censorAPI = require('./src/api/censor')
app.use('/api', censorAPI)
 
app.listen(port, () => {
    console.log(`listening at port ${port}`);
})