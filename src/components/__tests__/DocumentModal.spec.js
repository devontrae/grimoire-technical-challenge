import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import DocumentModal from '../DocumentModal.vue'

describe('DocumentModal', () => {
  it('renders properly', () => {
    const wrapper = mount(DocumentModal, { props: { documentData: 'Lorem Ipsum' }})
    expect(wrapper.find('.documentData').element.value).toContain('Lorem Ipsum')
  })
})
