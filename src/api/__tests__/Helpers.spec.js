import { describe, it, expect } from 'vitest'

import { parseCensorInput } from '../helpers/document.helper'

describe('ParseCensorInput', () => {
  it('parses properly', () => {
    const test_input = `"mollis metus" "lorem ipsum", dolor sit, 'consectetur', 'Phasellus vitae'`
    const expected_output = [
        'mollis metus',
        'lorem ipsum',
        'dolor',
        'sit',
        'consectetur',
        'Phasellus vitae'
    ]
    expect(parseCensorInput(test_input)).toEqual(expected_output)
  })
})
