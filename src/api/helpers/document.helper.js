module.exports.parseCensorInput = censorInput => {
    let censors = []

    // First filter censors by comma
    let censorsCSV = censorInput.split(',')
    
    // A function to extract the text between single/double quotes and removes them, and returns only space delimited values
    const extractPhrases = str => {
        let strRegEx = new RegExp(`("|')(.*?)('|")`, 'gi')
        let match
        let leftover_str = str
        while ((match = strRegEx.exec(str)) != null) {
            let phrase = match[0]
            let phraseText = match[2]
            censors.push(phraseText)

            leftover_str = leftover_str.replace(phrase, '')
        }

        return leftover_str.trim();
    }

    // Now, go through each comma split censor, and see if it ends with a single or double quote to see if its a phrase
    for(let i = 0; i < censorsCSV.length; i++) {
        let censor = censorsCSV[i].trim()

        // Extract the phrases into the censors array
        censor = extractPhrases(censor)
         
        if (censor.length > 0) {
            censor.split(' ').map(keyword => {
                censors.push(keyword)
            })
        }
    }
    console.log('censors', censors)
    return censors
}