const express = require('express')
const router = express.Router()
const path = require('path')
const multer = require('multer')
const fs = require('fs')
const { parseCensorInput } = require('./helpers/document.helper')

// File Upload middleware
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let uploadErrors = [];
        if (file.mimetype !== 'text/plain') {
            uploadErrors.push({ message: 'Invalid file type. Must be of mime type text/plain'})
        }

        if (uploadErrors.length > 0) {
            cb(JSON.stringify(uploadErrors))
        } else {
            cb(null, 'tmp')
        }
    },
    filename: (req, file, cb) => {
        // console.log(file)
        cb(null, file.originalname)
    }
})

const upload = multer({ storage })

router.post('/upload', upload.single('document'), async (req, res) => {
    const censorInput = req.body.censor || false
    // Lets get the file contents
    let document = fs.readFileSync(req.file.path).toString()

    if (!censorInput) {
        return res.status(400).send('You must include text you\'d like censored.')
    }

    let censors = parseCensorInput(censorInput) // Test created for this helper function
    
    // console.log('censors', censors)
    // Iterate over censors and replace them with XXXX
    for(let i = 0; i < censors.length; i++) {
        let censor = censors[i].trim()
        if (censor !== '') { // Prevent empty censors from passing
            let regex = new RegExp(censor, 'gi')
            document = regex[Symbol.replace](document, 'XXXX')
        }
    } 

    // Remove the tmp file
    fs.unlink(req.file.path, (err) => {
        if (err) {
            console.error(err)
        }
        res.send(document) 
    })
})

module.exports = router;