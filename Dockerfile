FROM node

LABEL name=signaling-svc \
      version=1.0.0 \
      maintainer="Devontrae Walls <vont@medik8mobile.com>"

WORKDIR /usr/src/app

COPY . .

RUN npm install -g pm2
RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
